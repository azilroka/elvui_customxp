## Interface: 50300
## Title: |cff1784d1ElvUI |rCustom XP
## Author: Sortokk
## Version: 1.13
## Notes: Custom XP Tags for use with ElvUI Custom Text
## RequiredDeps: ElvUI
## OptionalDeps: Flourish

libs\load_libs.xml
ElvUI_CustomXP.lua
